package org.semanticweb.owlapi.apibinding.configurables;

/**
 * @author ignazio
 *
 * @param <V> computed type
 */
public abstract class ComputableAllThrowables<V> implements Computable<V> {
    protected Throwable exception;

    @Override
    public boolean hasThrownException() {
        return exception != null;
    }

    @Override
    public Throwable thrownException() {
        return exception;
    }
}
