package uk.ac.manchester.cs.chainsaw;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;

import utils.cachedreasoner.CachedOWLReasoner;

public class JFactChainsawReasonerFactory implements OWLReasonerFactory {
	private final OWLReasonerFactory factory;

	public JFactChainsawReasonerFactory() {
		this("uk.ac.manchester.cs.jfact.JFactFactory");
	}

	public JFactChainsawReasonerFactory(String factoryName) {
		try {
			factory = (OWLReasonerFactory) Class.forName(factoryName).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Cannot find " + factoryName + " on the classpath", e);
		}
	}

	public String getReasonerName() {
		return "Chainsaw_JFact";
	}

	public OWLReasoner createReasoner(OWLOntology ontology) {
		JFactChainsawReasoner toReturn = new JFactChainsawReasoner(factory, ontology, new SimpleConfiguration());
		return verify(toReturn);
	}

	private OWLReasoner verify(ChainsawReasoner toReturn) {
		OWLOntologyManager m = toReturn.getRootOntology().getOWLOntologyManager();
		final CachedOWLReasoner cachedOWLReasoner = new CachedOWLReasoner(toReturn, m);
		m.addOntologyChangeListener(toReturn);
		m.addOntologyChangeListener(cachedOWLReasoner);
		return cachedOWLReasoner;
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology ontology) {
		JFactChainsawReasoner toReturn = new JFactChainsawReasoner(factory, ontology, new SimpleConfiguration());
		return verify(toReturn);
	}

	public OWLReasoner createReasoner(OWLOntology ontology, OWLReasonerConfiguration config)
			throws IllegalConfigurationException {
		JFactChainsawReasoner toReturn = new JFactChainsawReasoner(factory, ontology, config);
		return verify(toReturn);
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology ontology, OWLReasonerConfiguration config)
			throws IllegalConfigurationException {
		JFactChainsawReasoner toReturn = new JFactChainsawReasoner(factory, ontology, config);
		return verify(toReturn);
	}
}
