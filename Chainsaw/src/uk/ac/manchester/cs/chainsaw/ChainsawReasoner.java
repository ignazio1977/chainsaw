package uk.ac.manchester.cs.chainsaw;

import java.util.*;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.profiles.*;
import org.semanticweb.owlapi.reasoner.*;
import org.semanticweb.owlapi.reasoner.impl.*;
import org.semanticweb.owlapi.reasoner.knowledgeexploration.OWLKnowledgeExplorerReasoner;
import org.semanticweb.owlapi.util.Version;

import uk.ac.manchester.cs.atomicdecomposition.*;
import uk.ac.manchester.cs.atomicdecomposition.nativeimpl.AtomicDecomposerNativeImpl;
import uk.ac.manchester.cs.factplusplus.owlapiv3.*;

public class ChainsawReasoner implements OWLReasoner, OWLKnowledgeExplorerReasoner, OWLOntologyChangeListener {
	private final class LRU100MapReasoners extends LinkedHashMap<Set<OWLAxiom>, OWLReasoner> {
		private static final long serialVersionUID = 180194115253481043L;

		@Override
		protected boolean removeEldestEntry(Map.Entry<java.util.Set<OWLAxiom>, OWLReasoner> eldest) {
			if (size() > max) {
				eldest.getValue().dispose();
				return true;
			}
			return false;
		}
	}

	private final class LRU100Map extends LinkedHashMap<java.util.Set<OWLEntity>, Set<OWLAxiom>> {

		private static final long serialVersionUID = 180194115253481043L;

		@Override
		protected boolean removeEldestEntry(Map.Entry<java.util.Set<OWLEntity>, Set<OWLAxiom>> eldest) {
			if (size() > max) {

				return true;
			}
			return false;
		}
	}

	enum PapillonType {
		TOP, BOTTOM, ALL
	}

	protected final OWLOntology rootOntology;
	private OWLReasonerFactory delegateFactory;
	private final OWLDataFactory df = OWLManager.getOWLDataFactory();
	private final OWLReasonerConfiguration config;
	private AtomicDecomposition ad = null;
	private final ArrayIntMap topCone = new ArrayIntMap();
	private final ArrayIntMap bottomCone = new ArrayIntMap();
	private final List<OWLAxiom> axiomsList = new ArrayList<OWLAxiom>();
	private final List<OWLEntity> entityList = new ArrayList<OWLEntity>();
	Map<OWLEntity, Integer> entityIndex = new HashMap<OWLEntity, Integer>();
	Map<OWLAxiom, Integer> axiomIndex = new HashMap<OWLAxiom, Integer>();
	private FaCTPlusPlusReasoner modularisationReasoner = null;
	private ChainsawClassifier Classifier;
	final long startTime;

	public ChainsawReasoner(OWLReasonerFactory f, OWLOntology o, OWLReasonerConfiguration config) {
		rootOntology = o;
		startTime = System.currentTimeMillis();
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		for (OWLOntology ont : rootOntology.getImportsClosure()) {
			for (OWLAxiom ax : ont.getAxioms()) {
				axioms.add(ax.getAxiomWithoutAnnotations());
			}
		}
		axiomsList.addAll(axioms);
		entityList.addAll(rootOntology.getSignature(true));
		for (int i = 0; i < entityList.size(); i++) {
			entityIndex.put(entityList.get(i), i);
		}
		for (int i = 0; i < axiomsList.size(); i++) {
			axiomIndex.put(axiomsList.get(i), i);
		}
		delegateFactory = f;
		this.config = config;
		initModularisationReasoner(rootOntology);
	}

	//
	// Internal modularisation interface
	//

	protected void initModularisationReasoner(OWLOntology o) {
		if (modularisationReasoner != null) {
			modularisationReasoner.dispose();
		}
		modularisationReasoner = (FaCTPlusPlusReasoner) new FaCTPlusPlusReasonerFactory().createReasoner(o);
	}

	protected Set<OWLAxiom> moduleStrategy(Set<OWLEntity> sig, boolean b, int moduleTechnique) {
		final Set<OWLAxiom> module = modularisationReasoner.getModule(sig, b, moduleTechnique);
		return module;
	}

	protected Set<OWLAxiom> getModule(Set<OWLEntity> _sig) {
		Set<OWLEntity> sig = new HashSet<OWLEntity>();
		for (OWLEntity e : _sig) {
			if (!e.isBuiltIn()) {
				sig.add(e);
			}
		}
		Set<OWLAxiom> module = modulesCache.get(sig);
		if (module == null) {
			module = moduleStrategy(sig, false, 0);
			modulesCache.put(sig, module);
		}
		return module;
	}

	//
	// AD creation interface
	//

	private final List<OWLDeclarationAxiom> declarations = new ArrayList<OWLDeclarationAxiom>();

	private void initAtomicDecomposition() {
		declarations.clear();
		Set<OWLEntity> entities = new HashSet<OWLEntity>();
		for (OWLEntity e : entityList) {
			if (!e.isBuiltIn() && !e.isOWLNamedIndividual()) {
				declarations.add(df.getOWLDeclarationAxiom(e));
				entities.add(e);
			}
		}
		topCone.clear();
		bottomCone.clear();
		ad = getAtomicDecomposer();
		for (Atom atom : ad.getAtoms()) {
			Collection<OWLEntity> signature = computeSignature(atom);
			entities.removeAll(signature);
			computeLabel(atom, signature);
			fillTopCone(signature, atom);
			fillBotCone(signature, atom);
			for (Atom d : ad.getDependencies(atom)) {
				fillBotCone(signature, d);
			}
			for (Atom d : ad.getDependents(atom)) {
				fillTopCone(signature, d);
			}
		}
		// generate the taxonomy
		Classifier = new ChainsawClassifier(this, ad);
	}

	public void fillTopCone(Collection<OWLEntity> signature, Atom atom) {
		for (OWLAxiom ax : atom.getAxioms()) {
			for (OWLEntity e : signature) {
				topCone.put(entityIndex.get(e), axiomIndex.get(ax));
			}
		}
	}

	public void fillBotCone(Collection<OWLEntity> signature, Atom atom) {
		for (OWLAxiom ax : atom.getAxioms()) {
			for (OWLEntity e : signature) {
				bottomCone.put(entityIndex.get(e), axiomIndex.get(ax));
			}
		}
	}

	protected AtomicDecomposition getAtomicDecomposer() {
		return new AtomicDecomposerNativeImpl(rootOntology, 0);
	}

	public Set<OWLAxiom> asSet(FastSet axioms) {
		Set<OWLAxiom> set = new HashSet<OWLAxiom>();
		for (int i = 0; i < axioms.size(); i++) {
			set.add(axiomsList.get(axioms.get(i)));
		}
		return set;
	}

	public Collection<OWLEntity> computeSignature(Atom atom) {
		Collection<OWLEntity> signature = atom.getSignature();
		List<OWLEntity> toRemove = new ArrayList<OWLEntity>();
		for (OWLEntity e : signature) {
			if (e.isBuiltIn()) {
				toRemove.add(e);
			}
		}
		signature.removeAll(toRemove);
		return signature;
	}

	private void computeLabel(Atom atom, Collection<OWLEntity> signature) {
		Iterator<Atom> dependencies = ad.getDependencies(atom).iterator();
		Set<OWLEntity> labelSignature = new HashSet<OWLEntity>(signature);
		while (labelSignature.size() > 0 && dependencies.hasNext()) {
			Atom a = dependencies.next();
			if (!a.equals(atom)) {
				labelSignature.removeAll(a.getSignature());
			}
		}
		atom.setLabel(labelSignature);
	}

	public String getReasonerName() {
		return "Chainsaw";
	}

	public Version getReasonerVersion() {
		return new Version(0, 1, 0, 0);
	}

	public BufferingMode getBufferingMode() {
		return BufferingMode.BUFFERING;
	}

	public void flush() {
		initAtomicDecomposition();
		nextChanges.clear();
	}

	private final List<OWLOntologyChange> nextChanges = new ArrayList<OWLOntologyChange>();

	public void ontologiesChanged(List<? extends OWLOntologyChange> changes) throws OWLException {
		nextChanges.addAll(changes);
	}

	public List<OWLOntologyChange> getPendingChanges() {
		return new ArrayList<OWLOntologyChange>(nextChanges);
	}

	public Set<OWLAxiom> getPendingAxiomAdditions() {
		Set<OWLAxiom> added = new HashSet<OWLAxiom>();
		for (OWLOntologyChange c : nextChanges) {
			if (c.isAddAxiom()) {
				added.add(c.getAxiom());
			}
		}
		return added;
	}

	public static void main(String[] args) {
	}

	public Set<OWLAxiom> getPendingAxiomRemovals() {
		Set<OWLAxiom> added = new HashSet<OWLAxiom>();
		for (OWLOntologyChange c : nextChanges) {
			if (c.isRemoveAxiom()) {
				added.add(c.getAxiom());
			}
		}
		return added;
	}

	public OWLOntology getRootOntology() {
		return rootOntology;
	}

	public void interrupt() {
		// TODO Auto-generated method stub
	}

	public void precomputeInferences(InferenceType... inferenceTypes) throws ReasonerInterruptedException,
			TimeOutException, InconsistentOntologyException {
		initAtomicDecomposition();
	}

	public boolean isPrecomputed(InferenceType inferenceType) {
		return false;
	}

	public Set<InferenceType> getPrecomputableInferenceTypes() {
		return Collections.emptySet();
	}

	public boolean isConsistent() throws ReasonerInterruptedException, TimeOutException {
		return getDelegate(getModule(Collections.<OWLEntity> emptySet()), false).isConsistent();
	}

	public boolean isSatisfiable(OWLClassExpression classExpression) throws ReasonerInterruptedException,
			TimeOutException, ClassExpressionNotInProfileException, FreshEntitiesException,
			InconsistentOntologyException {
		if (!classExpression.isAnonymous() && Classifier != null) {
			return !getBottomClassNode().contains(classExpression.asOWLClass());
		}
		return getDelegate(buildPapillon(PapillonType.TOP, classExpression.getSignature())).isSatisfiable(
				classExpression);
	}

	private Set<OWLAxiom> buildPapillon(PapillonType type, OWLEntity... signature) {
		return buildPapillon(type, Arrays.asList(signature));
	}

	private Set<OWLAxiom> buildPapillon(PapillonType type, Collection<? extends OWLEntity> signature) {
		try {
			Set<OWLAxiom> toReturn = new HashSet<OWLAxiom>();
			for (OWLEntity e : signature) {
				if (entityIndex.containsKey(e)) {
					if (type == PapillonType.TOP || type == PapillonType.ALL) {
						toReturn.addAll(asSet(topCone.get(entityIndex.get(e))));
					}
					if (type == PapillonType.BOTTOM || type == PapillonType.ALL) {
						toReturn.addAll(asSet(bottomCone.get(entityIndex.get(e))));
					}
				} else {
					// fresh entity or owl:thing or similar. Skip
				}
			}
			toReturn.addAll(declarations);
			return toReturn;
		} catch (NullPointerException e) {
			System.out.println("ChainsawReasoner.buildPapillon() null");
			e.printStackTrace();
			throw e;
		}
	}

	private final int max = 20;
	private final LinkedHashMap<Set<OWLAxiom>, OWLReasoner> reasonersHardCache = new LRU100MapReasoners();
	protected final LinkedHashMap<Set<OWLEntity>, Set<OWLAxiom>> modulesCache = new LRU100Map();

	OWLReasoner getDelegate(Set<OWLAxiom> axioms) {
		return getDelegate(axioms, true);
	}

	private void verify(OWLOntology o) {
		OWL2DLProfile profile = new OWL2DLProfile();
		OWLProfileReport report = profile.checkOntology(o);
		for (OWLProfileViolation v : report.getViolations()) {
			System.out.println(v);
		}
	}

	protected OWLReasoner getDelegate(Set<OWLAxiom> axioms, boolean precompute) {
		if (axioms.isEmpty()) {
		}
		OWLReasoner reasoner = reasonersHardCache.get(axioms);
		try {
			if (reasoner == null) {
				// System.out.println("KnowledgeExplorerGraph.getReasoner() cache miss");
				final OWLOntologyManager m = OWLManager.createOWLOntologyManager();
				OWLOntology temp = m.createOntology(axioms);
				// verify(temp);
				reasoner = delegateFactory.createReasoner(temp, config);
				reasonersHardCache.put(axioms, reasoner);
			}
			return reasoner;
		} catch (OWLOntologyCreationException e) {
			throw new RuntimeException("Cannot create the ontology", e);
		}
	}

	public Node<OWLClass> getUnsatisfiableClasses() throws ReasonerInterruptedException, TimeOutException,
			InconsistentOntologyException {
		return getBottomClassNode();
	}

	public boolean isEntailed(OWLAxiom axiom) throws ReasonerInterruptedException, UnsupportedEntailmentTypeException,
			TimeOutException, AxiomNotInProfileException, FreshEntitiesException, InconsistentOntologyException {
		final Set<OWLAxiom> module = getModule(axiom.getSignature());
		// System.out.println("ChainsawReasoner.isEntailed() "+module.size()+"\t"+axiom);
		return getDelegate(module).isEntailed(axiom);
	}

	public boolean isEntailed(Set<? extends OWLAxiom> axioms) throws ReasonerInterruptedException,
			UnsupportedEntailmentTypeException, TimeOutException, AxiomNotInProfileException, FreshEntitiesException,
			InconsistentOntologyException {
		// TODO investigate a tradeoff on grouping the axioms together
		for (OWLAxiom ax : axioms) {
			if (!isEntailed(ax)) {
				return false;
			}
		}
		return true;
	}

	public boolean isEntailmentCheckingSupported(AxiomType<?> axiomType) {
		return true;
	}

	private ChainsawClassifier getClassifier() {
		if (Classifier == null) {
			initAtomicDecomposition();
		}
		return Classifier;
	}

	public Node<OWLClass> getTopClassNode() {
		return getEquivalentClasses(df.getOWLThing());
	}

	public Node<OWLClass> getBottomClassNode() {
		return getEquivalentClasses(df.getOWLNothing());
	}

	public NodeSet<OWLClass> getSubClasses(OWLClassExpression ce, boolean direct) throws ReasonerInterruptedException,
			TimeOutException, FreshEntitiesException, InconsistentOntologyException,
			ClassExpressionNotInProfileException {
		if (ce.isBottomEntity()) {
			return new OWLClassNodeSet();
		}
		if (!ce.isAnonymous()) {
			return getClassifier().getSubClasses((OWLClass) ce, direct);
		}

		return getDelegate(buildPapillon(PapillonType.BOTTOM, ce.getSignature())).getSubClasses(ce, direct);
	}

	public NodeSet<OWLClass> getSuperClasses(OWLClassExpression ce, boolean direct)
			throws InconsistentOntologyException, ClassExpressionNotInProfileException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		if (ce.isTopEntity()) {
			return new OWLClassNodeSet();
		}
		if (!ce.isAnonymous()) {
			return getClassifier().getSuperClasses((OWLClass) ce, direct);
		}
		return getDelegate(buildPapillon(PapillonType.TOP, ce.getSignature())).getSuperClasses(ce, direct);
	}

	public Node<OWLClass> getEquivalentClasses(OWLClassExpression ce) throws InconsistentOntologyException,
			ClassExpressionNotInProfileException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		if (!ce.isAnonymous()) {
			return getClassifier().getEquivalentClasses((OWLClass) ce);
		}

		return getDelegate(buildPapillon(PapillonType.TOP, ce.getSignature())).getEquivalentClasses(ce);
	}

	public NodeSet<OWLClass> getDisjointClasses(OWLClassExpression ce) throws ReasonerInterruptedException,
			TimeOutException, FreshEntitiesException, InconsistentOntologyException {
		Set<OWLClass> allentities = new HashSet<OWLClass>(rootOntology.getClassesInSignature(true));
		Set<OWLClass> foundDisjoints = new HashSet<OWLClass>();
		Set<Node<OWLClass>> foundNodes = new HashSet<Node<OWLClass>>();
		for (OWLClass d : allentities) {
			if (!foundDisjoints.contains(d)) {
				if (!isSatisfiable(df.getOWLObjectIntersectionOf(ce, d))) {
					// if the intersection is not satisfiable, the two class
					// expressions are disjoint
					// therefore, all equivalent classes to d are disjoint
					// and all its sublclasses
					final Node<OWLClass> equivalentClasses = getEquivalentClasses(d);
					foundNodes.add(equivalentClasses);
					foundDisjoints.addAll(equivalentClasses.getEntities());
					NodeSet<OWLClass> subClasses = getSubClasses(d, false);
					foundNodes.addAll(subClasses.getNodes());
					for (Node<OWLClass> n : subClasses.getNodes()) {
						foundDisjoints.addAll(n.getEntities());
					}
				}
			}
		}
		return new OWLClassNodeSet(foundNodes);
	}

	public Node<OWLObjectPropertyExpression> getTopObjectPropertyNode() {
		return getEquivalentObjectProperties(df.getOWLTopObjectProperty());
	}

	public Node<OWLObjectPropertyExpression> getBottomObjectPropertyNode() {
		return getEquivalentObjectProperties(df.getOWLBottomObjectProperty());
	}

	public NodeSet<OWLObjectPropertyExpression> getSubObjectProperties(OWLObjectPropertyExpression pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getSubObjectProperties(pe, direct);
	}

	public NodeSet<OWLObjectPropertyExpression> getSuperObjectProperties(OWLObjectPropertyExpression pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getSuperObjectProperties(pe, direct);
	}

	public Node<OWLObjectPropertyExpression> getEquivalentObjectProperties(OWLObjectPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getEquivalentObjectProperties(pe);
	}

	public NodeSet<OWLObjectPropertyExpression> getDisjointObjectProperties(OWLObjectPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getDisjointObjectProperties(pe);
	}

	public Node<OWLObjectPropertyExpression> getInverseObjectProperties(OWLObjectPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getInverseObjectProperties(pe);
	}

	public NodeSet<OWLClass> getObjectPropertyDomains(OWLObjectPropertyExpression pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		Set<OWLEntity> sig = new HashSet<OWLEntity>(pe.getSignature());
		Set<OWLEntity> newsig = new HashSet<OWLEntity>();
		NodeSet<OWLClass> toReturn;
		do {
			newsig.addAll(sig);
			toReturn = getDelegate(buildPapillon(PapillonType.ALL, sig)).getObjectPropertyDomains(pe, direct);
			sig.addAll(toReturn.getFlattened());
		} while (!sig.equals(newsig));
		return toReturn;
	}

	public NodeSet<OWLClass> getObjectPropertyRanges(OWLObjectPropertyExpression pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		Set<OWLEntity> sig = new HashSet<OWLEntity>(pe.getSignature());
		Set<OWLEntity> newsig = new HashSet<OWLEntity>();
		NodeSet<OWLClass> toReturn;
		do {
			newsig.addAll(sig);
			toReturn = getDelegate(buildPapillon(PapillonType.ALL, sig)).getObjectPropertyRanges(pe, direct);
			sig.addAll(toReturn.getFlattened());
		} while (!sig.equals(newsig));
		return toReturn;
	}

	public Node<OWLDataProperty> getTopDataPropertyNode() {
		OWLDataPropertyNode toReturn = new OWLDataPropertyNode();
		toReturn.add(df.getOWLTopDataProperty());
		return toReturn;
	}

	public Node<OWLDataProperty> getBottomDataPropertyNode() {
		return getEquivalentDataProperties(df.getOWLBottomDataProperty());
	}

	public NodeSet<OWLDataProperty> getSubDataProperties(OWLDataProperty pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getSubDataProperties(pe, direct);
	}

	public NodeSet<OWLDataProperty> getSuperDataProperties(OWLDataProperty pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getSuperDataProperties(pe, direct);
	}

	public Node<OWLDataProperty> getEquivalentDataProperties(OWLDataProperty pe) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException, TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getEquivalentDataProperties(pe);
	}

	public NodeSet<OWLDataProperty> getDisjointDataProperties(OWLDataPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, pe.getSignature())).getDisjointDataProperties(pe);
	}

	public NodeSet<OWLClass> getDataPropertyDomains(OWLDataProperty pe, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		Set<OWLEntity> sig = new HashSet<OWLEntity>(pe.getSignature());
		Set<OWLEntity> newsig = new HashSet<OWLEntity>();
		NodeSet<OWLClass> toReturn;
		do {
			newsig.addAll(sig);
			toReturn = getDelegate(buildPapillon(PapillonType.ALL, sig)).getDataPropertyDomains(pe, direct);
			sig.addAll(toReturn.getFlattened());
		} while (!sig.equals(newsig));
		return toReturn;
	}

	public NodeSet<OWLClass> getTypes(OWLNamedIndividual ind, boolean direct) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException, TimeOutException {
		final Set<OWLAxiom> buildPapillon = buildPapillon(PapillonType.ALL, ind.getSignature());
		// System.out.println("ChainsawReasoner.getTypes() " + ind + "\t" +
		// direct);
		// for (OWLAxiom ax : buildPapillon) {
		// System.out.println("ChainsawReasoner.getTypes() " + ax);
		// }
		// for (OWLOntology o : rootOntology.getImportsClosure()) {
		// for (OWLAxiom a : o.getAxioms()) {
		// if (a.getSignature().containsAll(ind.getSignature())) {
		// System.out.println("def: " + a);
		// }
		// }
		// }
		return getDelegate(buildPapillon).getTypes(ind, direct);
	}

	public NodeSet<OWLNamedIndividual> getInstances(OWLClassExpression ce, boolean direct)
			throws InconsistentOntologyException, ClassExpressionNotInProfileException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		if (ce.isTopEntity()) {
			Set<Node<OWLNamedIndividual>> set = new HashSet<Node<OWLNamedIndividual>>();
			for (OWLNamedIndividual n : rootOntology.getIndividualsInSignature(true)) {
				set.add(new OWLNamedIndividualNode(n));
			}
			return new OWLNamedIndividualNodeSet(set);
		}
		if (ce.isBottomEntity()) {
			return new OWLNamedIndividualNodeSet();
		}
		return getDelegate(buildPapillon(PapillonType.BOTTOM, ce.getSignature())).getInstances(ce, direct);
	}

	public NodeSet<OWLNamedIndividual> getObjectPropertyValues(OWLNamedIndividual ind, OWLObjectPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		Set<OWLAxiom> props = buildPapillon(PapillonType.ALL, pe.getSignature());
		props.addAll(buildPapillon(PapillonType.ALL, ind.getSignature()));
		return getDelegate(props).getObjectPropertyValues(ind, pe);
	}

	public Set<OWLLiteral> getDataPropertyValues(OWLNamedIndividual ind, OWLDataProperty pe)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		Set<OWLAxiom> props = buildPapillon(PapillonType.ALL, pe.getSignature());
		props.addAll(buildPapillon(PapillonType.ALL, ind.getSignature()));
		return getDelegate(props).getDataPropertyValues(ind, pe);
	}

	public Node<OWLNamedIndividual> getSameIndividuals(OWLNamedIndividual ind) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException, TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, ind.getSignature())).getSameIndividuals(ind);
	}

	public NodeSet<OWLNamedIndividual> getDifferentIndividuals(OWLNamedIndividual ind)
			throws InconsistentOntologyException, FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		return getDelegate(buildPapillon(PapillonType.ALL, ind.getSignature())).getDifferentIndividuals(ind);
	}

	public long getTimeOut() {
		return config.getTimeOut();
	}

	public FreshEntityPolicy getFreshEntityPolicy() {
		return config.getFreshEntityPolicy();
	}

	public IndividualNodeSetPolicy getIndividualNodeSetPolicy() {
		return config.getIndividualNodeSetPolicy();
	}

	public void dispose() {
		ad = null;
		delegateFactory = null;
		for (OWLReasoner r : reasonersHardCache.values()) {
			r.dispose();
		}
		declarations.clear();
		reasonersHardCache.clear();
		topCone.clear();
		bottomCone.clear();
		rootOntology.getOWLOntologyManager().removeOntologyChangeListener(this);
	}

	//
	// Knowledge exploration part
	//

	private List<OWLKnowledgeExplorerReasoner> explorationReasoners = new ArrayList<OWLKnowledgeExplorerReasoner>();
	private Map<RootNode, OWLKnowledgeExplorerReasoner> explorationCache = new HashMap<OWLKnowledgeExplorerReasoner.RootNode, OWLKnowledgeExplorerReasoner>();

	protected OWLKnowledgeExplorerReasoner getExplorationDelegate(Set<OWLAxiom> axioms) {
		OWLKnowledgeExplorationReasonerWrapper reasoner = new OWLKnowledgeExplorationReasonerWrapper(
				(FaCTPlusPlusReasoner) getDelegate(axioms, true));
		explorationReasoners.add(reasoner);
		return reasoner;
	}

	public void clearKnowledgeExplorationReasonersCache() {
		// free all the reasoners
		for (OWLKnowledgeExplorerReasoner reasoner : explorationReasoners) {
			reasoner.dispose();
		}
		explorationReasoners.clear();
		explorationCache.clear();
	}

	@Override
	public RootNode getRoot(OWLClassExpression expression) {
		OWLKnowledgeExplorerReasoner explorationDelegate = getExplorationDelegate(buildPapillon(PapillonType.ALL,
				expression.getSignature()));
		RootNode root = explorationDelegate.getRoot(expression);
		explorationCache.put(root, explorationDelegate);
		return root;
	}

	@Override
	public Node<? extends OWLObjectPropertyExpression> getObjectNeighbours(RootNode node, boolean deterministicOnly) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			return owlKnowledgeExplorerReasoner.getObjectNeighbours(node, deterministicOnly);
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public Node<OWLDataProperty> getDataNeighbours(RootNode node, boolean deterministicOnly) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			return owlKnowledgeExplorerReasoner.getDataNeighbours(node, deterministicOnly);
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public Collection<RootNode> getObjectNeighbours(RootNode node, OWLObjectProperty property) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			Collection<RootNode> toReturn = owlKnowledgeExplorerReasoner.getObjectNeighbours(node, property);
			// all these nodes have the same reasoner
			for (RootNode n : toReturn) {
				explorationCache.put(n, owlKnowledgeExplorerReasoner);
			}
			return toReturn;
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public Collection<RootNode> getDataNeighbours(RootNode node, OWLDataProperty property) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			return owlKnowledgeExplorerReasoner.getDataNeighbours(node, property);
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public Node<? extends OWLClassExpression> getObjectLabel(RootNode node, boolean deterministicOnly) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			return owlKnowledgeExplorerReasoner.getObjectLabel(node, deterministicOnly);
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public Node<? extends OWLDataRange> getDataLabel(RootNode node, boolean deterministicOnly) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			return owlKnowledgeExplorerReasoner.getDataLabel(node, deterministicOnly);
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	@Override
	public RootNode getBlocker(RootNode node) {
		OWLKnowledgeExplorerReasoner owlKnowledgeExplorerReasoner = explorationCache.get(node);
		if (owlKnowledgeExplorerReasoner != null) {
			RootNode blocker = owlKnowledgeExplorerReasoner.getBlocker(node);
			explorationCache.put(blocker, owlKnowledgeExplorerReasoner);
			return blocker;
		}
		throw new RuntimeException("No reasoner cached for this node, call getRoot on your class expression again: "
				+ node);
	}

	public AtomicDecomposition getAtomicDecomposition() {
		return ad;
	}
}
