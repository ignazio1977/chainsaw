package uk.ac.manchester.cs.atomicdecomposition.nativeimpl;

import java.util.*;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;
import org.semanticweb.owlapi.util.MultiMap;

import uk.ac.manchester.cs.atomicdecomposition.*;
import uk.ac.manchester.cs.chainsaw.*;
import uk.ac.manchester.cs.factplusplus.owlapiv3.FaCTPlusPlusReasoner;

public class AtomicDecomposerNativeImpl implements AtomicDecomposition {
	Set<OWLAxiom> globalAxioms;
	Set<OWLAxiom> tautologies;
	final MultiMap<OWLEntity, Atom> termBasedIndex = new MultiMap<OWLEntity, Atom>();
	List<Atom> atoms = new ArrayList<Atom>();
	Map<Atom, Integer> atomIndex = new HashMap<Atom, Integer>();
	ArrayIntMap dependents = new ArrayIntMap();
	ArrayIntMap dependencies = new ArrayIntMap();
	FaCTPlusPlusReasoner delegate;
	private final int type;

	public AtomicDecomposerNativeImpl(OWLOntology o, int t) {
		type = t;
		delegate = new FaCTPlusPlusReasoner(o, new SimpleConfiguration(), BufferingMode.NON_BUFFERING);
		int size = delegate.getAtomicDecompositionSize(false, type);
		for (int i = 0; i < size; i++) {
			final Atom atom = new Atom(delegate.getAtomAxioms(i));
			atoms.add(atom);
			atomIndex.put(atom, i);
			for (OWLEntity e : atom.getSignature()) {
				termBasedIndex.put(e, atom);
			}
		}
		for (int i = 0; i < size; i++) {
			int[] dependentIndexes = delegate.getAtomDependents(i);
			for (int j : dependentIndexes) {
				dependencies.put(i, j);
				dependents.put(j, i);
			}
		}
	}

	public int getModuleType() {
		return type;
	}

	@Override
	public Set<Atom> getAtoms() {
		return new HashSet<Atom>(atoms);
	}

	@Override
	public Atom getAtomForAxiom(OWLAxiom axiom) {
		for (int i = 0; i < atoms.size(); i++) {
			if (atoms.get(i).contains(axiom)) {
				return atoms.get(i);
			}
		}
		return null;
	}

	@Override
	public boolean isTopAtom(Atom atom) {
		return !dependents.containsKey(atomIndex.get(atom));
	}

	@Override
	public boolean isBottomAtom(Atom atom) {
		return !dependencies.containsKey(atomIndex.get(atom));
	}

	@Override
	public Set<OWLAxiom> getPrincipalIdeal(Atom atom) {
		return delegate.getAtomModule(atomIndex.get(atom));
	}

	@Override
	public Set<OWLEntity> getPrincipalIdealSignature(Atom atom) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Atom> getDependencies(Atom atom) {
		return getDependencies(atom, false);
	}

	@Override
	public Set<Atom> getDependencies(Atom atom, boolean direct) {
		return explore(atom, direct, dependencies);
	}

	@Override
	public Set<Atom> getDependents(Atom atom) {
		return getDependents(atom, false);
	}

	@Override
	public Set<Atom> getDependents(Atom atom, boolean direct) {
		return explore(atom, direct, dependents);
	}

	private Set<Atom> explore(Atom atom, boolean direct, ArrayIntMap multimap) {
		if (direct) {
			return asSet(multimap.get(atomIndex.get(atom)));
		}
		Set<Atom> toReturn = new HashSet<Atom>();
		toReturn.add(atom);
		List<Atom> toDo = new ArrayList<Atom>();
		toDo.add(atom);
		for (int i = 0; i < toDo.size(); i++) {
			final Integer key = atomIndex.get(toDo.get(i));
			if (key == null) {
				System.out.println("AtomicDecomposerNativeImpl.explore()");
			}
			FastSet c = multimap.get(key);
			for (int j = 0; j < c.size(); j++) {
				Atom a = atoms.get(c.get(j));
				if (toReturn.add(a)) {
					toDo.add(a);
				}
			}
		}
		return toReturn;
	}

	@Override
	public Set<Atom> getRelatedAtoms(Atom atom) {
		Set<Atom> s = getDependencies(atom);
		s.addAll(getDependents(atom));
		return s;
	}

	@Override
	public Set<Atom> getTopAtoms() {
		Set<Atom> keys = getAtoms();
		for (Integer i : dependencies.getAllValues()) {
			keys.remove(atoms.get(i));
		}
		return keys;
	}

	private Set<Atom> asSet(Iterable<Integer> keys) {
		Set<Atom> s = new HashSet<Atom>();
		for (int i : keys) {
			s.add(atoms.get(i));
		}
		return s;
	}

	private Set<Atom> asSet(FastSet keys) {
		Set<Atom> s = new HashSet<Atom>();
		for (int i = 0; i < keys.size(); i++) {
			s.add(atoms.get(keys.get(i)));
		}
		return s;
	}

	@Override
	public Set<Atom> getBottomAtoms() {
		Set<Atom> keys = getAtoms();
		for (Integer i : dependents.getAllValues()) {
			keys.remove(atoms.get(i));
		}
		return keys;
	}

	@Override
	public Map<OWLEntity, Set<Atom>> getTermBasedIndex() {
		Map<OWLEntity, Set<Atom>> toReturn = new HashMap<OWLEntity, Set<Atom>>();
		for (OWLEntity e : termBasedIndex.keySet()) {
			toReturn.put(e, new HashSet<Atom>(termBasedIndex.get(e)));
		}
		return toReturn;
	}

	@Override
	public Set<OWLAxiom> getTautologies() {
		return tautologies;
	}
}
